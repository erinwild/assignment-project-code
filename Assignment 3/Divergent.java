import java.math.*;

public class Divergent {
    /* Erin Wild
     * Assignment 3
     * October 7, 2014
     * Purpose: to find point w that diverges but takes the largest number of steps to diverge using the Julia parameter z=-0.47+0.55i
     */
    public static final double[] z = {-0.47,0.55};
    public static final int popSize = 1000;
    public static double[][] w;
    public static double[] fitness;
    public static int[] sortingIndex;
    
    public static final int mevs = 100000;
    public static final int escapeLimit = 10000000;
    
    public static MersenneTwisterFast rng;
    
    public static void main(String[] args) {
        
        for(int forever = 0; forever < 1000000; forever++) {
            rng = new MersenneTwisterFast();
            
            /* Initialize population */
            w = new double[2][popSize];
            fitness = new double[popSize];
            sortingIndex = new int[popSize];
            double lo = -1;
            double hi = 1;
            for(int j = 0; j < popSize/2; j++) {
                sortingIndex[j] = j;
                for(int i = 0; i < 2; i++) {
                    w[i][j] = rng.nextDouble()*(hi-lo)+lo;
                }
                fitness[j] = escape(w[0][j],w[0][j]);
            }
            for(int j = popSize/2; j < popSize; j++) {
                sortingIndex[j] = j;
                for(int i = 0; i < 2; i++) {
                    w[i][j] = rng.nextDouble()*(hi-lo)+lo;
                }
                fitness[j] = escape(w[0][j],w[0][j]);
            }
            
            /* Mating events */
            int tSize = 7;
            for(int t = 0; t < mevs; t++) {
                tournament(tSize, popSize); // select for tournament
                
                // replace worst with best
                w[0][sortingIndex[0]] = w[0][sortingIndex[tSize-1]];
                w[1][sortingIndex[0]] = w[1][sortingIndex[tSize-1]];
                w[0][sortingIndex[1]] = w[0][sortingIndex[tSize-2]];
                w[1][sortingIndex[1]] = w[1][sortingIndex[tSize-2]];
                
                // uniform crossover
                double swap;
                if(rng.nextDouble() >= 0.5) {
                    swap = w[0][sortingIndex[0]];
                    w[0][sortingIndex[0]] = w[0][sortingIndex[1]];
                    w[0][sortingIndex[1]] = swap;
                }
                if(rng.nextDouble() >= 0.5) {
                    swap = w[1][sortingIndex[0]];
                    w[1][sortingIndex[0]] = w[1][sortingIndex[1]];
                    w[1][sortingIndex[1]] = swap;
                }
                
                // mutation
                for(int m = 0; m < rng.nextInt(2); m++) {
                    w[rng.nextInt(2)][sortingIndex[0]] = rng.nextDouble()*(hi-lo)+lo;
                }
                for(int m = 0; m < rng.nextInt(2); m++) {
                    w[rng.nextInt(2)][sortingIndex[1]] = rng.nextDouble()*(hi-lo)+lo;
                }
                
                // update fitness
                fitness[sortingIndex[0]] = escape(w[0][sortingIndex[0]],w[1][sortingIndex[0]]);
                fitness[sortingIndex[1]] = escape(w[0][sortingIndex[1]],w[1][sortingIndex[1]]);
                fitness[sortingIndex[2]] = escape(w[0][sortingIndex[1]],w[1][sortingIndex[1]]);
            }
            
            /* Report best */
            double best = -1;
            int index = -1;
            for(int j = 1; j < popSize; j++) {
                if(fitness[j] > best) {
                    best = fitness[j];
                    index = j;
                }
            }
                System.out.println(forever + ": w = " + w[0][index] + " + " + w[1][index] + "i : " + fitness[index]);

        }
    }
    
    public static int escape(double w1, double w2) {
        double z1 = z[0]; // real part of sequence
        double z2 = z[1]; // complex part of sequence
        double tempz1;
        for(int div = 0; div < escapeLimit; div++) {
            if((z1*z1)+(z2*z2)>4) {
                return div;
            }
            tempz1 = (z1*z1)-(z2*z2)+w1;
            z2 = 2*z1*z2+w2;
            z1 = tempz1;
        }
        return 0;
    }
    
    public static void tournament(int tSize, int pSize) {
        int t;
        int swap;
        for(int i = 0; i < tSize; i++) {
            t = rng.nextInt(pSize); // select random tournament members
            swap = sortingIndex[i];
            sortingIndex[i] = sortingIndex[t];
            sortingIndex[t] = swap; // swap tournament members to front of sorting array
        }
        
        // sort the tournament into lowest fitness first
        do {
            t = 0;
            for(int i = 0; i < tSize - 1; i++) {
                if(fitness[sortingIndex[i]] > fitness[sortingIndex[i+1]]) {
                    // if out of order, swap
                    swap = sortingIndex[i];
                    sortingIndex[i] = sortingIndex[i+1];
                    sortingIndex[i+1] = swap;
                    t = 1;
                }
            }
        } while(t == 1);
    }
}
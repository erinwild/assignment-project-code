import sys
import argparse
import random

fixed = False
parser = argparse.ArgumentParser()
parser.add_argument("--fixed", action="store_true", dest="fixed")

num = int(sys.argv[1])
if fixed:
    x = int(sys.argv[2])
    y = int(sys.argv[3])
else:
    min = int(sys.argv[2])
    max = int(sys.argv[3])

boards = open(sys.argv[4],'w')
boards.write(str(num)+"\n")

counter = 0
while counter<num:
    if fixed is False:
        x = random.randint(min,max)
        y = random.randint(min,max)
    boards.write(str(x)+" "+str(y)+"\n")
    for i in xrange(x):
        if (i==0 or i==x-1):
            for j in xrange(y):
                boards.write("2")
            boards.write("\n")
        else:
            for j in xrange(y):
                if (j==0 or j==y-1):
                    boards.write("2")
                elif (i==1 and j==1):
                    boards.write("0")
                else:
                    cell = random.randint(1,10)
                    if (cell<=2):
                        boards.write("2")
                    else:
                        boards.write("0")
            boards.write("\n")
    counter = counter + 1
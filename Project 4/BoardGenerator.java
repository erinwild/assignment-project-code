import java.io.*;

public class BoardGenerator {
    public static MersenneTwisterFast rng;
    public static final int boardMax = 12;
    public static final int boardMin = 7;
    public static final int numBoards = 500;
    
    public static void main(String[] args) throws FileNotFoundException {
        rng = new MersenneTwisterFast();
        File fileBoards = new File("boards.txt");
        PrintWriter printerBoards = new PrintWriter(fileBoards);
        
        printerBoards.println(numBoards);
        for(int board = 0; board < numBoards; board++) {
            int boardLength = rng.nextInt(boardMax + 1 - boardMin) + boardMin;
            int boardWidth = rng.nextInt(boardMax + 1 - boardMin) + boardMin;
            
            printerBoards.println(boardLength + " " + boardWidth);
            // top row
            for(int i = 0; i < boardLength; i++) {
                printerBoards.print("2");
            }
            printerBoards.println("");
            // second row
            printerBoards.print("20"); // keeping corner clear for bot
            for(int i = 0; i < boardLength-3; i++) {
                if(rng.nextInt(10) > 7) {
                    printerBoards.print("2"); // 20% of the time add an obstacle
                } else {
                    printerBoards.print("0");
                }
            }
            printerBoards.println("2");
            // rows 3 to boardWidth-1
            for(int j = 0; j < boardWidth-3; j++) {
                printerBoards.print("2");
                for(int i = 0; i < boardLength-2; i++) {
                    if(rng.nextInt(10) > 7) {
                        printerBoards.print("2"); // 20% of the time add an obstacle
                    } else {
                        printerBoards.print("0");
                    }
                }
                printerBoards.println("2");
            }
            // bottom row
            for(int i = 0; i < boardLength; i++) {
                printerBoards.print("2");
            }
            printerBoards.println("");
        }
        printerBoards.close();
    }
}